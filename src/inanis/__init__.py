# /bin/env python
# -*- encode: utf-8 -*-
__author__ = '@britodfbr'
from pathlib import Path


with open(Path('.').joinpath('version.txt').resolve()) as f:
    __version__ = f.read().strip()


def main():
    print(f'v: {__version__}')


if __name__ == '__main__':
    print(main())
